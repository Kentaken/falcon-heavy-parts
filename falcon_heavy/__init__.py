from __future__ import unicode_literals

import logging


LOGGER_NAME = 'falcon_heavy'

logger = logging.getLogger(LOGGER_NAME)
